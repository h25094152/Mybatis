package com.somta.mybatis.demo.example.helloworld;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;

import com.somta.mybatis.demo.dao.UserDao;
import com.somta.mybatis.demo.pojo.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

/**
 * Created by Kevin on 2019/1/11.
 */
public class MybatisHelloWorld {
    public static void main(String[] args) {

        String resource = "mybatis-config.xml";
        try {
            InputStream inputStream = Resources.getResourceAsStream(resource);
            //通过SqlSessionFactoryBuilder创建
            SqlSessionFactory sqlMapper = new  SqlSessionFactoryBuilder().build(inputStream);
            SqlSession session = sqlMapper.openSession();
            try {
                //第一种方式
                User user = (User) session.selectOne("com.somta.mybatis.demo.dao.UserDao.getUser", 1);
                System.out.println(user.getId() + "," + user.getName());
            } finally {
                session.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
